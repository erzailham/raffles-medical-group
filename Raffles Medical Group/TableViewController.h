//
//  TableViewController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/13/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@end
