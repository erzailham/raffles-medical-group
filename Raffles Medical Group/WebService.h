//
//  WebService.h
//  Unilever-Portal-News
//
//  Created by Reza Ilham Mubaroq on 2/4/16.
//  Copyright © 2016 com.unilever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@interface WebService : NSObject {
    AFHTTPRequestOperation *operation;
}

@property (strong, nonatomic) UIWindow *window;

- (void)postAPI:(NSString *)action withParam:(NSMutableDictionary *)param withLoading:(BOOL)isLoading success:(void (^)(NSDictionary *result))success;

- (void)getAPI:(NSString *)action withParam:(NSMutableDictionary *)param withLoading:(BOOL)isLoading success:(void (^)(NSDictionary *result))success;

@end
