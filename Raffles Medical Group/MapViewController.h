//
//  MapViewController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 7/9/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) NSString *latitude;
@property (weak, nonatomic) NSString *longitude;


@end
