//
//  WebviewController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/2/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebviewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) NSString *link;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;

@end
