//
//  ReadArticleController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/1/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadArticleController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) NSString *articleID;
@property (weak, nonatomic) NSString *titleNav;
//@property (weak, nonatomic) IBOutlet UIScrollView *backScroll;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleNavItem;
@property (weak, nonatomic) IBOutlet UIWebView *webview;


@end
