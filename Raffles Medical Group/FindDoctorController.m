//
//  FindDoctorController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/7/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "FindDoctorController.h"
#import "XMLDictionary.h"
#import "DetailDoctorController.h"
#import "Config.h"

@interface FindDoctorController (){
    NSArray *data;
    NSArray *doctor;
    NSMutableArray *dataDoctor;
    NSMutableArray *dataClinicDoctor;
}

@end

@implementation FindDoctorController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataDoctor = [[NSMutableArray alloc] init];
    dataClinicDoctor = [[NSMutableArray alloc] init];
    doctor = [[NSMutableArray alloc] init];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_back"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 15, 15);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIImage *image = [UIImage imageNamed:@"logo-text"];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:image];
    [titleImageView setFrame:CGRectMake(0, 0, 20, 20)];
    [titleImageView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = titleImageView;
    
    [self getClicicDoctor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getClicicDoctor {
    NSArray *doctorID  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicDoctor"];
    NSString *clinic_id = self.third;
    
    if (![clinic_id isEqualToString:@""]) {
        doctor = [doctorID filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINIC_ID == %@)", clinic_id]];
    }else {
        doctor = doctorID;
    }
    
    [self getDataDoctor];
}

- (void)getDataDoctor {
    data  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListDoctorData"];;
    NSArray *datas;
    for (id object in doctor) {
        NSString *doctor_id = [object valueForKey:@"DOCTOR_ID"];
        NSArray *filter = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(DOCTOR_ID == %@)", doctor_id]];
        
        if ([filter count]>0) {
            [dataDoctor addObjectsFromArray:filter];
            datas = [dataDoctor mutableCopy];
        }
    }
    
    if (![self.second isEqualToString:@""]) {
        datas = [datas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SPECIALITY == %@)", self.second]];
    }
    
    if (![self.first isEqualToString:@""]) {
        datas = [datas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(NAME CONTAINS[cd] %@)", self.first]];
    }
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:datas];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    NSArray *doctorID  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicDoctor"];
    NSDictionary *xmlDocs = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicData"]];
    NSArray *doctorClinic = [xmlDocs objectForKey:@"CLINIC"];
    
    for (id object in arrayWithoutDuplicates) {
        NSArray *search = [doctorID filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(DOCTOR_ID == %@)", [object objectForKey:@"DOCTOR_ID"]]];
        
        NSString *clinicId = [[search valueForKey:@"CLINIC_ID"] objectAtIndex:0];
        NSArray *filter = [doctorClinic filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINIC_ID == %@)", clinicId]];
        
        
        [dataClinicDoctor addObject:filter];
    }
    
    
//    
    [dataDoctor setArray:arrayWithoutDuplicates];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataDoctor.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSString *url = [[dataDoctor valueForKey:@"PHOTO"] objectAtIndex:indexPath.row];
    url = [url stringByReplacingOccurrencesOfString:@" "
                                           withString:@"%20"];
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    cell.textLabel.text = [[dataDoctor valueForKey:@"NAME"] objectAtIndex:indexPath.row];
    
    NSDictionary *a = [[dataClinicDoctor objectAtIndex:indexPath.row] objectAtIndex:0];
    
    cell.detailTextLabel.text = [a valueForKey:@"CLINICNAME"];
    
    [cell.detailTextLabel setTextColor:[UIColor colorWithRed:0.04 green:0.35 blue:0.25 alpha:1.0]];
    
    [cell.imageView.layer setCornerRadius:30];
    cell.imageView.layer.masksToBounds = YES;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailDoctorController *vc = (DetailDoctorController *)[sb instantiateViewControllerWithIdentifier:@"detailDoctor"];
    vc.dataDoctor = [dataDoctor objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
