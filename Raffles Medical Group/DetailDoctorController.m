//
//  DetailDoctorController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/6/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "DetailDoctorController.h"

@interface DetailDoctorController ()

@end

@implementation DetailDoctorController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_back"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 15, 15);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    NSString *url = [self.dataDoctor valueForKey:@"PHOTO"];
    url = [url stringByReplacingOccurrencesOfString:@" "
                                         withString:@"%20"];
    self.photo.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    self.photo.layer.cornerRadius = 40;
    self.photo.layer.masksToBounds = YES;
    self.name.text = [self.dataDoctor valueForKey:@"NAME"];
    self.clinicalContent.text = [self.dataDoctor valueForKey:@"CLINICINTEREST"];
    self.profileContent.text = [self.dataDoctor valueForKey:@"PROFILE"];
    
    NSString *htmlString = [NSString stringWithFormat:@"<style> body { font-family:helvetica !important; font-size: 11pt !important;} </style> %@",[self.dataDoctor valueForKey:@"PROFILE"]];
    [self.profileWebview loadHTMLString:htmlString baseURL:nil];
    self.profileWebview.delegate = self;
    //[self.profileWebview setScalesPageToFit:YES];
}

- (void)goback {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CGFloat contentHeight = webView.scrollView.contentSize.height;
    
    self.heightContent.constant = contentHeight;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
