//
//  LaunchViewController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/23/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "LaunchViewController.h"
#import <Chameleon.h>
#import "Config.h"
#import "XMLDictionary.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "WebService.h"

@interface LaunchViewController () {
    WebService *webservice;
    NSMutableDictionary *param;
}

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    webservice = [[WebService alloc] init];
    param = [[NSMutableDictionary alloc] init];

    [self getConfig];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getConfig {
    [webservice getAPI:GETCONFIG withParam:param withLoading:YES success:^(NSDictionary *result) {
//        if (result != NULL) {
            NSString *splash = [result objectForKey:@"SplashScreen"];
            splash = [splash stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            NSString *LatestHappening = [result objectForKey:@"LatestHappening"];
            LatestHappening = [LatestHappening stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            NSString *Package = [result objectForKey:@"Package"];
            Package = [Package stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            NSString *PatientEducation = [result objectForKey:@"PatientEducation"];
            PatientEducation = [PatientEducation stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            NSString *SearchScreen = [result objectForKey:@"SearchScreen"];
            SearchScreen = [SearchScreen stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            [self.imageView setImageWithURL:[NSURL URLWithString:splash] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            [[NSUserDefaults standardUserDefaults] setObject:LatestHappening forKey:@"LatestHappening"];
            [[NSUserDefaults standardUserDefaults] setObject:Package forKey:@"Package"];
            [[NSUserDefaults standardUserDefaults] setObject:PatientEducation forKey:@"PatientEducation"];
            [[NSUserDefaults standardUserDefaults] setObject:SearchScreen forKey:@"SearchScreen"];
            [[NSUserDefaults standardUserDefaults] setObject:splash forKey:@"SplashScreen"];
            
            [self getCategory];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)getCategory {
    ///Category
    [webservice getAPI:CATEGORYURL withParam:param withLoading:YES success:^(NSDictionary *result) {
//        if (result != NULL) {
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"Vw_Category_Company"] forKey:@"ListCategory"];
            
            NSMutableArray *serviceImage = [[NSMutableArray alloc] init];
            for (id object in [result objectForKey:@"Vw_Category_Company"]) {
                NSString *CAT_ICON = [object valueForKey:@"CAT_ICON"];
                NSString *urlImage = [CAT_ICON stringByReplacingOccurrencesOfString:@" "withString:@"%20"];
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:[NSURL URLWithString:urlImage]
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         
                                     }
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                        if (image) {
                                            NSData* data = UIImagePNGRepresentation(image);
                                            [serviceImage addObject:data];
                                            [[NSUserDefaults standardUserDefaults] setObject:serviceImage forKey:@"serviceImage"];
                                        }
                                    }];
            }
            [self subCategory];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)subCategory {
    NSURL *URL = [[NSURL alloc] initWithString:SUBCATEGORYURL];
    NSString *xmlString = [[NSString alloc] initWithContentsOfURL:URL encoding:NSUTF8StringEncoding error:NULL];
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:xmlString];
    NSArray *subcat = [xmlDoc valueForKey:@"SUBCAT"];
    [[NSUserDefaults standardUserDefaults] setObject:subcat forKey:@"ListSubCategory"];
    
    [self getClinicData];
}

- (void)getClinicData {
    NSURL *URL = [[NSURL alloc] initWithString:GETCLINICURL];
    NSString *xmlString = [[NSString alloc] initWithContentsOfURL:URL encoding:NSUTF8StringEncoding error:NULL];
   
    [[NSUserDefaults standardUserDefaults] setObject:xmlString forKey:@"ListClinicData"];
    
    [self ClinicDoctor];
}

- (void)ClinicDoctor {
    [webservice getAPI:GETCLINICDOCTOR withParam:param withLoading:YES success:^(NSDictionary *result) {
//        if (result != NULL) {
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"CLINIC_DOCTOR"] forKey:@"ListClinicDoctor"];
            [self ListDoctorData];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)ListDoctorData {
    [webservice getAPI:GETDOCTORDATA withParam:param withLoading:YES success:^(NSDictionary *result) {
//        if (result != NULL) {
           [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"DOCTOR"] forKey:@"ListDoctorData"];
           [self GetSpecial];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)GetSpecial {
    [webservice getAPI:GETSPECIALITY withParam:param withLoading:NO success:^(NSDictionary *result) {
//        if (result != NULL) {
            NSMutableArray *string = [result objectForKey:@"string"];
            [string removeObjectAtIndex:0];
            [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"ListSpeciality"];
            [self GetAreaClinic7];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)GetAreaClinic7 {
    NSURL *URLAreaClinic = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@/%@",GETAREACLINICURL,@"CA-0000007"]];
    NSString *xmlStringAreaClinic = [[NSString alloc] initWithContentsOfURL:URLAreaClinic encoding:NSUTF8StringEncoding error:NULL];
    NSDictionary *xmlDocAreaClinic = [NSDictionary dictionaryWithXMLString:xmlStringAreaClinic];
    [[NSUserDefaults standardUserDefaults] setObject:xmlStringAreaClinic forKey:@"AreaClinic7String"];
    NSArray *location = [[xmlDocAreaClinic objectForKey:@"TemplateArea"] valueForKey:@"Kode"];
    NSMutableArray *a = [[NSMutableArray alloc] init];
    
    for (id object in location) {
        [a addObject:object];
    }
    [a removeObjectIdenticalTo:[NSNull null]];
    
    [[NSUserDefaults standardUserDefaults] setObject:a forKey:@"AreaClinic7"];
    [self GetAreaClinic2];
}

- (void)GetAreaClinic2 {
    NSURL *URLAreaClinic2 = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@/%@",GETAREACLINICURL,@"CA-0000002"]];
    NSString *xmlStringAreaClinic2 = [[NSString alloc] initWithContentsOfURL:URLAreaClinic2 encoding:NSUTF8StringEncoding error:NULL];
    NSDictionary *xmlDocAreaClinic2 = [NSDictionary dictionaryWithXMLString:xmlStringAreaClinic2];
    [[NSUserDefaults standardUserDefaults] setObject:xmlStringAreaClinic2 forKey:@"AreaClinic2String"];
    [[NSUserDefaults standardUserDefaults] setObject:[xmlDocAreaClinic2 objectForKey:@"TemplateArea"] forKey:@"AreaClinic2"];
    [self GetPatientEdu];
}

- (void)GetPatientEdu {
    [webservice getAPI:GETPATIENTEDU withParam:param withLoading:YES success:^(NSDictionary *result) {
//        if (result != NULL) {
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"TemplateCategorywithArticle"] forKey:@"ListPatientEdu"];
            [self GetLatestHap];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)GetLatestHap {
    [webservice getAPI:GETLATESTHAP withParam:param withLoading:YES success:^(NSDictionary *result) {
//        if (result != NULL) {
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"TemplateCategorywithArticle"] forKey:@"ListLatestHap"];
            [self GetPackage];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)GetPackage {
    [webservice getAPI:GETPACKAGE withParam:param withLoading:YES success:^(NSDictionary *result) {
//        if (result != NULL) {
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"TemplateCategorywithArticle"] forKey:@"ListPackage"];
            [self toHome];
//        }
//        else {
//            [self connectionError];
//        }
    }];
}

- (void)toHome {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"navHome"];
    [ivc setModalPresentationStyle:UIModalPresentationCustom];
    [ivc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:ivc animated:YES completion:nil];
}

- (void)connectionError {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"Please check your internet connection or try again later"
                                                   delegate:self
                                          cancelButtonTitle:@"Retry"
                                          otherButtonTitles:nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]){
        [self getConfig];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
