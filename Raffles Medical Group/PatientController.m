//
//  PatientController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/13/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "PatientController.h"
#import "WebviewController.h"
#import "PatientTableController.h"
#import <Chameleon.h>

@interface PatientController () {
    CAPSPageMenu *pageMenu;
}

@end

@implementation PatientController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    //PAGE MENU
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    PatientTableController *controller1 = (PatientTableController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"PatientTable"];
    controller1.title = @"HEALTHNEWS";
    controller1.parentNavigationController = self.navigationController;
    
    WebviewController *controller2 = (WebviewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"WebviewController"];
    controller2.title = @"SUBSCRIBE";
    
    NSArray *controllerArray = @[controller1, controller2];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: HexColor(@"075A40"),
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor blackColor],
                                 CAPSPageMenuOptionMenuHeight: @(50.0),
                                 CAPSPageMenuOptionMenuMargin: @(0.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(width/2),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: HexColor(@"075A40")
                                 };
    
    pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.viewPatient.frame.size.width, self.viewPatient.frame.size.height) options:parameters];
    [pageMenu.view setBackgroundColor:[UIColor clearColor]];
    pageMenu.delegate = self;
    [self.viewPatient addSubview:pageMenu.view];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_home"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(toHome:) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 20, 20);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.rightBarButtonItem = backButton;
}

//Page Menu Method
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    if (index == 0) {
        
    } else {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toHome:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *vc = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"navHome"];
    [self presentViewController:vc animated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
