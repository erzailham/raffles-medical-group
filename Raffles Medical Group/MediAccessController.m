//
//  MediAccessController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/13/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "MediAccessController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface MediAccessController () {
    UIActivityIndicatorView *loadingIndicator;
    NSString *undoLink;
    NSString *currentLink;
}

@end

@implementation MediAccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-10, (self.view.frame.size.height/2)-80, 20,20)];
    [loadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [loadingIndicator setHidesWhenStopped:YES];
    [self.webView addSubview:loadingIndicator];
    
    //self.navigationItem.hidesBackButton = YES;
    NSString *fullURL = @"https://www.rafflesmedicalonline.com/mymediaccess/Account";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
    [self.webView loadRequest:requestObj];
    self.webView.delegate = self;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_home"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(toHome:) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 20, 20);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.rightBarButtonItem = backButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [loadingIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [loadingIndicator stopAnimating];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    [NSHTTPCookieStorage sharedHTTPCookieStorage].cookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways ;
    
    NSLog(@"ini urlnya %@",[request.URL absoluteString]);
    
    currentLink = [request.URL absoluteString];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[request.URL absoluteString]]];
    [self toHome:nil];
    
    return YES;
}

- (void)toHome:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *vc = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"navHome"];
    [self presentViewController:vc animated:YES completion:NULL];
}

-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if ( self.presentedViewController)
    {
        [super dismissViewControllerAnimated:flag completion:completion];
    }
}

- (IBAction)undoLink:(id)sender {
    [self.webView goBack];
}

- (IBAction)redoLink:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
