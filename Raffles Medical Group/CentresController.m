//
//  CentresController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/30/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "CentresController.h"
#import "CentresDetailController.h"
#import "CentresCell.h"
#import "PageMenuController.h"
#import "XMLDictionary.h"
#import "Config.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface CentresController () {
    NSMutableArray *data;
    NSMutableArray *distanceSortedArray;
    NSString *currentTime;
    NSDateFormatter *dateFormatter;
    NSDate *now;
    
    CLLocation *currentLocation;
    
    CLLocationManager *locationManager;
    
    NSArray *i;
    
    BOOL getLocation;
}

@end

@implementation CentresController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *headerView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 151)];
    
    NSString *image = self.categoryImage;
    image = [image stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [headerView setImageWithURL:[NSURL URLWithString:image] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    headerView.contentMode = UIViewContentModeScaleAspectFill;
    
    now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh.mm"];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh.mma"];
    
    getLocation = YES;
    
    currentTime = [dateFormatter stringFromDate:now];
    
    [self getClinicData];
    
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setEstimatedRowHeight:60];
    [self.tableView setTableHeaderView:headerView];
    [self getOperationHour];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getClinicData {
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicData"]];
    NSLog(@"dictionary: %@", xmlDoc);
    
    data  = [xmlDoc objectForKey:@"CLINIC"];
///    data  = [xmlDoc objectForKey:@"TemplateClinicApi"];
    i  = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SUBCATEGORY_ID == %@)", self.categoryId]];
    
    data = [[NSMutableArray alloc] init];
    for (id object in i) {
        [data addObject:object];
    }
    
    [self getNearby];
}

- (void)getNearby {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
    //[self calculateDistace:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.latitude];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);

    if (getLocation) {
        getLocation = NO;
        [self calculateDistace:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    }
}

- (void)calculateDistace:(double)latitude longitude:(double)longitude {
    CLLocation *placeLocation;
    CLLocation *userLocation;
    
    userLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    distanceSortedArray = [[NSMutableArray alloc] init];
    
    for (id object in data) {
        
        int distance = 0;
        
        double lati = [[object valueForKey:@"LATITUDE"] doubleValue];
        double longi = [[object valueForKey:@"LONGITUDE"] doubleValue];
        placeLocation = [[CLLocation alloc] initWithLatitude:lati longitude:longi];
        distance = [userLocation distanceFromLocation:placeLocation];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic = object;
        
        distance = distance/1000;
        
        //nsmutabledictionary = [[NSMutableDictionary alloc] initWithObjects:listVolumn forKeys:listname];
        [dic setValue:[NSNumber numberWithInt:distance] forKey:@"distance"];
        
        [distanceSortedArray addObject:dic];
        
        NSLog(@"dic=%@",dic );
    }
    
    data = distanceSortedArray;
    
    [self getOperationHour];
}

- (void)getOperationHour {
    NSMutableArray *dataoperation = [[NSMutableArray alloc] init];
    NSUInteger count = 0;
    for (id object in data) {
        
        NSString *operationHour;
        
        NSString *day;
        NSString *Nameday;
        NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
        switch ([component weekday]) {
            case 1:
                //Sunday
                day = @"OPHRS_SUN";
                Nameday =@"Sunday";
                break;
            case 2:
                //Monday
                day = @"OPHRS_MON";
                Nameday =@"Monday";
                break;
            case 3:
                //Tuesday
                day = @"OPHRS_TUE";
                Nameday =@"Tuesday";
                break;
            case 4:
                //Wednesday
                day = @"OPHRS_WED";
                Nameday = @"Wednesday";
                break;
            case 5:
                //Thursday
                day = @"OPHRS_THURS";
                Nameday = @"Thursday";
                break;
            case 6:
                //Friday
                day = @"OPHRS_FRI";
                Nameday = @"ThFridayursday";
                break;
            case 7:
                //Saturday
                day = @"OPHRS_SAT";
                Nameday = @"Saturday";
                break;
            default:
                break;
        }
        
        
        NSArray *assa = [i valueForKey:@"ListOperationDetail"];
        assa = [assa valueForKey:@"TemplateClinicOperatingHoursReturn"];
        assa = [assa valueForKey:@"ListOperatignHours"];
        assa = [[assa valueForKey:@"TemplateOperatingHoursDetail"] objectAtIndex:0];
        
        NSString *a;
        if (![assa isEqual:[NSNull null]]) {
            for (id object in assa) {
                NSString *name = [object valueForKey:@"DayName"];
                if ([Nameday isEqualToString:name]) {
                    a = [object valueForKey:@"OperatingHours"];
                }
            }
        }else {
            a = [[i valueForKey:day] objectAtIndex:count];
        }
        
        if (a == NULL) {
            a = [[i valueForKey:day] objectAtIndex:count];
        }
        
        if (![a isKindOfClass:[NSNull class]]) {
            if ([a isEqualToString:@"Open 24 hours"] || [a isEqualToString:@"open 24 hours"] || [a isEqualToString:@"Open 24 Hours"] || [a isEqualToString:@"midnight"] || [a isEqualToString:@"Midnight"] || [a isEqualToString:@"12.00mn"] || [a isEqualToString:@"12.00Mn"] || [a isEqualToString:@"12.00MN"] || [a isEqualToString:@"12.00 mn"] || [a isEqualToString:@"12.00 Mn"] || [a isEqualToString:@"12.00 MN"]) {
                operationHour = @"open";
            }else if ([a isEqualToString:@"Closed"]) {
                operationHour = @"closed";
            } else {
                a = [a stringByReplacingOccurrencesOfString:@"," withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"–" withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"?" withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"to" withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"  " withString:@" "];
                NSString *start = [a substringToIndex:7];
                start = [start stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSString *end = [a substringWithRange:NSMakeRange([a length]-7, 7)];
                end = [end stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                NSDate *startDate = [dateFormatter dateFromString:start];
                NSDate *endDate = [dateFormatter dateFromString:end];
                
                if ([self isTimeOfDate:now betweenStartDate:startDate andEndDate:endDate]) {
                    NSLog(@"TARGET IS INSIDE!");
                    operationHour = @"open";
                }else {
                    NSLog(@"TARGET IS NOT INSIDE!");
                    if (endDate == nil) {
                        operationHour = @"open";
                    }else {
                        operationHour = @"closed";
                    }
                }
            }
        } else {
            operationHour = @"closed";
        }
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic = object;
        [dic setValue:operationHour forKey:@"operationHour"];
        
        [dataoperation addObject:dic];
        count ++;
    }
    
    data = dataoperation;
    [self.tableView reloadData];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return data.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CentresCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
    CentresCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        [cell2.nearbyButton addTarget:self action:@selector(sortNearby) forControlEvents:UIControlEventTouchUpInside];
        [cell2.AlfaButton addTarget:self action:@selector(sortName) forControlEvents:UIControlEventTouchUpInside];
        
        return cell2;
    }else if (indexPath.section == 1) {
        NSString *clinicName = [[data valueForKey:@"CLINICNAME"] objectAtIndex:indexPath.row];
        clinicName = [clinicName stringByReplacingOccurrencesOfString:@"å" withString:@"'"];
        cell1.clinicName.text = clinicName;
        cell1.address.text = [[data valueForKey:@"ADDRESS"] objectAtIndex:indexPath.row];
        
        NSString *dst = [[data valueForKey:@"distance"] objectAtIndex:indexPath.row];
        if (![dst isEqual:[NSNull null]]) {
            NSString *distance = [[[data valueForKey:@"distance"] objectAtIndex:indexPath.row] stringValue];
            if ([distance isEqualToString:@"0"] || [[[data valueForKey:@"LATITUDE"] objectAtIndex:indexPath.row] integerValue] == 0 || [[[data valueForKey:@"LONGITUDE"] objectAtIndex:indexPath.row] integerValue] == 0) {
                cell1.distance.hidden = NO;
                cell1.distance.text = @"0km";
            }else {
                cell1.distance.hidden = NO;
                cell1.distance.text = [NSString stringWithFormat:@"%@km",[[[data valueForKey:@"distance"] objectAtIndex:indexPath.row] stringValue]];
            }
        }
        
        NSString *opr = [[data valueForKey:@"operationHour"] objectAtIndex:indexPath.row];
        if (![opr isEqual:[NSNull null]]) {
            cell1.trafictImage.image = [UIImage imageNamed:opr];
        }
        
        return cell1;
    }
    
    return cell1;
}

- (NSDate *)dateByNeutralizingDateComponentsOfDate:(NSDate *)originalDate {
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    // Get the components for this date
    NSDateComponents *components = [gregorian components:  (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate: originalDate];
    
    // Set the year, month and day to some values (the values are arbitrary)
    [components setYear:2000];
    [components setMonth:1];
    [components setDay:1];
    
    return [gregorian dateFromComponents:components];
}

- (BOOL)isTimeOfDate:(NSDate *)targetDate betweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate {
    if (!targetDate || !startDate || !endDate) {
        return NO;
    }
    
    // Make sure all the dates have the same date component.
    NSDate *newStartDate = [self dateByNeutralizingDateComponentsOfDate:startDate];
    NSDate *newEndDate = [self dateByNeutralizingDateComponentsOfDate:endDate];
    NSDate *newTargetDate = [self dateByNeutralizingDateComponentsOfDate:targetDate];
    
    // Compare the target with the start and end dates
    NSComparisonResult compareTargetToStart = [newTargetDate compare:newStartDate];
    NSComparisonResult compareTargetToEnd = [newTargetDate compare:newEndDate];
    
    return (compareTargetToStart == NSOrderedDescending && compareTargetToEnd == NSOrderedAscending);
}

- (void)sortNearby {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [distanceSortedArray sortUsingDescriptors:descriptors];
    data = distanceSortedArray;
    
    [self.tableView reloadData];
}

- (void)sortName {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"CLINICNAME" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [distanceSortedArray sortUsingDescriptors:descriptors];
    data = distanceSortedArray;
    
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CentresDetailController *vc = (CentresDetailController *)[sb instantiateViewControllerWithIdentifier:@"CentresDetail"];
        vc.data = [data objectAtIndex:indexPath.row];
        vc.categoryImage = self.categoryImage;
        [self.parentNavigationController pushViewController:vc animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
