//
//  FindDoctorController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/7/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindDoctorController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) NSString *first;
@property (weak, nonatomic) NSString *second;
@property (weak, nonatomic) NSString *third;

@end
