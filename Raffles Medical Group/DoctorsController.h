//
//  DoctorsController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/30/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorsController : UITableViewController

@property (nonatomic, strong) UINavigationController *parentNavigationController;
@property (nonatomic, strong) NSString *categoryId;

@end
