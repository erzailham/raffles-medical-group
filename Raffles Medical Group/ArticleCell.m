//
//  ArticcleCell.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/1/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "ArticleCell.h"

@implementation ArticleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.collapseView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)collapse:(id)sender {
    if([self.collapseButton.titleLabel.text isEqual: @"+"]){
        [self.collapseButton setTitle:@"-" forState:UIControlStateNormal];
    }
    else {
        [self.collapseButton setTitle:@"+" forState:UIControlStateNormal];
    }
}


@end
