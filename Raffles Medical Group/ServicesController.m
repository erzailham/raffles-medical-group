//
//  ServicesController.m
//  Raffles Medical Group
//
//  Created by Reza Ilham on 5/13/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "ServicesController.h"
#import "SpecialistController.h"
#import "PageMenuController.h"
#import "Config.h"
#import "XMLDictionary.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ServicesController () {
    NSMutableArray *data;
    NSArray *subCatdata;
    NSArray *serviceImage;
}

@end

@implementation ServicesController

- (void)viewDidLoad {
    [super viewDidLoad];
    data = [[NSMutableArray alloc] init];
    subCatdata = [[NSMutableArray alloc] init];
    [self getCategoryData];
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_home"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(toHome:) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 20, 20);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.rightBarButtonItem = backButton;
    
}

- (void)getCategoryData {
    data  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListCategory"];
    serviceImage = [[NSUserDefaults standardUserDefaults] objectForKey:@"serviceImage"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
 
    cell.textLabel.text = [[data valueForKey:@"CATEGORY"] objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageWithData:[serviceImage objectAtIndex:indexPath.row]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *categoryId = [[data valueForKey:@"CATEGORY_ID"] objectAtIndex:indexPath.row];
    NSArray *subCat = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListSubCategory"];
    subCat = [subCat filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CATEGORY_ID == %@)", categoryId]];
    
    if (subCat.count == 1) {
        PageMenuController *controller = [[PageMenuController alloc] init];
        controller.titleNavigation  = [[data valueForKey:@"CATEGORY"] objectAtIndex:indexPath.row];
        controller.categoryImage = [[data valueForKey:@"COMPANYLOGO"] objectAtIndex:indexPath.row];
        controller.categoryId = [[subCat valueForKey:@"SUBCATEGORY_ID"] objectAtIndex:0];
        [self.navigationController pushViewController:controller animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else {
        SpecialistController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Specialist"];
        controller.categoryId = [[data valueForKey:@"CATEGORY_ID"] objectAtIndex:indexPath.row];
        controller.categoryName = [[data valueForKey:@"CATEGORY"] objectAtIndex:indexPath.row];
        controller.categoryImage = [[data valueForKey:@"COMPANYLOGO"] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)toHome:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *vc = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"navHome"];
    [self presentViewController:vc animated:YES completion:NULL];
}

//#pragma mark - Navigation
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([[segue identifier] isEqualToString:@"toSpecialist"])
//    {
//        SpecialistController *vc = [segue destinationViewController];
//        
//    }
//}

@end
