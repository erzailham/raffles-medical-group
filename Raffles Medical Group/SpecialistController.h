//
//  SpecialistController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/16/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialistController : UITableViewController

@property (weak, nonatomic) NSString *categoryId;
@property (weak, nonatomic) NSString *categoryName;
@property (weak, nonatomic) NSString *categoryImage;

@end
