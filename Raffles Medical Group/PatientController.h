//
//  PatientController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/13/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"

@interface PatientController : UIViewController<CAPSPageMenuDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewPatient;

@end
