//
//  Config.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/29/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define GETCONFIG           @"https://mobileappsv2.rafflesmedical.com/services/GetConfiguration"

#define CATEGORYURL         @"https://mobileappsv2.rafflesmedical.com/services/GetCategoryData"

#define SUBCATEGORYURL      @"https://mobileappsv2.rafflesmedical.com/services/GetSubCategoryData"

#define GETCLINICURL        @"https://mobileappsv2.rafflesmedical.com/services/GetClinicData"

#define GETAREACLINICURL    @"https://mobileappsv2.rafflesmedical.com/services/GetAreaClinic"

#define GETCLINICDOCTOR     @"https://mobileappsv2.rafflesmedical.com/services/GetClinicDoctor"

#define GETDOCTORDATA       @"https://mobileappsv2.rafflesmedical.com/services/GetDoctorData"

#define GETSPECIALITY       @"https://mobileappsv2.rafflesmedical.com/services/GetSpeciality"

#define GETLATESTHAP        @"https://mobileappsv2.rafflesmedical.com/Services/GetLatestHappening"

#define GETPATIENTEDU       @"https://mobileappsv2.rafflesmedical.com/Services/GetPatientEducation"

#define GETPACKAGE          @"https://mobileappsv2.rafflesmedical.com/Services/GetPackage"

#define READARTICLE         @"https://mobileappsv2.rafflesmedical.com/Services/ReadArticle"

