//
//  OperationHourCell.m
//  Raffles Medical Group
//
//  Created by Reza Ilham on 9/22/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "OperationHourCell.h"

@implementation OperationHourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
