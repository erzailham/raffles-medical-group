//
//  PageMenuController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/30/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "PageMenuController.h"
#import "CentresController.h"
#import "DoctorsController.h"
#import <Chameleon.h>
#import "XMLDictionary.h"

@interface PageMenuController () {
    CAPSPageMenu *pageMenu;
    NSArray *data;
    NSMutableArray *doctor;
}

@end

@implementation PageMenuController

- (void)viewDidLoad {
    [super viewDidLoad];

    doctor = [[NSMutableArray alloc] init];
    
    [self.navigationItem setTitle:self.titleNavigation];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_back"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 15, 15);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self getClinicData];
    
    
}

- (void)getClinicData {
    
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicData"]];
    NSLog(@"dictionary: %@", xmlDoc);
    
    data  = [xmlDoc objectForKey:@"CLINIC"];
    data  = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SUBCATEGORY_ID == %@)", self.categoryId]];
    
    [self getClicicDoctor];
}

- (void)getClicicDoctor {
    NSArray *doctorID  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicDoctor"];
    for (id object in data) {
        NSArray *filter = [doctorID filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINIC_ID == %@)", [object valueForKey:@"CLINIC_ID"]]];
        
        if ([filter count]>0) {
            [doctor addObjectsFromArray:filter];
        }
    }
    
    //PAGE MENU
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    CentresController *controller1 = (CentresController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"Centres"];
    controller1.title = @"Centres";
    controller1.categoryImage = self.categoryImage;
    controller1.categoryId = self.categoryId;
    
    if ([self.message isEqualToString:@"detailCentres"]) {
        controller1.clicicId = self.clinicId;
        controller1.message  = self.message;
    }
    controller1.parentNavigationController = self.navigationController;
    
    DoctorsController *controller2 = (DoctorsController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"Doctors"];
    controller2.title = @"Doctors";
    controller2.categoryId = self.categoryId;
    controller2.parentNavigationController = self.navigationController;
    
    NSArray *controllerArray;
    NSDictionary *parameters;
    
    if ([doctor count] == 0) {
        controllerArray = @[controller1];
        parameters = @{
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionSelectionIndicatorColor: HexColor(@"075A40"),
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor blackColor],
                       CAPSPageMenuOptionMenuHeight: @(50.0),
                       CAPSPageMenuOptionMenuMargin: @(0.0),
                       CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width),
                       CAPSPageMenuOptionCenterMenuItems: @(YES),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: HexColor(@"075A40")
                       };
    }else {
        controllerArray = @[controller1, controller2];
        parameters = @{
                       CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                       CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                       CAPSPageMenuOptionSelectionIndicatorColor: HexColor(@"075A40"),
                       CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor blackColor],
                       CAPSPageMenuOptionMenuHeight: @(50.0),
                       CAPSPageMenuOptionMenuMargin: @(0.0),
                       CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width/2),
                       CAPSPageMenuOptionCenterMenuItems: @(YES),
                       CAPSPageMenuOptionSelectedMenuItemLabelColor: HexColor(@"075A40")
                       };
    }
    
    pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [pageMenu.view setBackgroundColor:[UIColor clearColor]];
    pageMenu.delegate = self;
    [self.view addSubview:pageMenu.view];
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

//Page Menu Method
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    if (index == 0) {
        
    } else {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
