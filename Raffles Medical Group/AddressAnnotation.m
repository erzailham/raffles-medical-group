//
//  AddressAnnotation.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/29/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "AddressAnnotation.h"

@implementation AddressAnnotation
@synthesize coordinate;

- (NSString *)subtitle{
    return nil;
}

- (NSString *)title{
    return nil;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c{
    coordinate=c;
    return self;
}

@end
