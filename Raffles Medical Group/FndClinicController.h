//
//  FndClinicController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/8/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FndClinicController : UITableViewController
@property (weak, nonatomic) NSString *first;
@property (weak, nonatomic) NSString *second;
@property (weak, nonatomic) NSString *third;
@end
