//
//  PatientTableController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/2/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatientTableController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIImageView *headerView;
@property (nonatomic, strong) UINavigationController *parentNavigationController;

@end
