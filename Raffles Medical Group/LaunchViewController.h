//
//  LaunchViewController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/23/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
