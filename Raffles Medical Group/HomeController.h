//
//  HomeController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/10/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *specialtyButton;
@property (weak, nonatomic) IBOutlet UIButton *centreButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *firstTextField;
@property (weak, nonatomic) IBOutlet UITextField *secondTextField;
@property (weak, nonatomic) IBOutlet UITextField *thirdTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIImageView *backHome;

@property (weak, nonatomic) IBOutlet UITextField *fourthTextField;
@property (weak, nonatomic) IBOutlet UITextField *fiveTextField;

@end
