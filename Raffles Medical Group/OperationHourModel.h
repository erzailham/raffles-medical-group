//
//  OperationHourModel.h
//  Raffles Medical Group
//
//  Created by Reza Ilham on 1/17/17.
//  Copyright © 2017 Raffles Medical Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OperationHourModel : NSObject
@property (nonatomic, readwrite, strong) NSString *operationHourTitle;
@property (nonatomic, readwrite, strong) NSArray *operationHour;
@end
