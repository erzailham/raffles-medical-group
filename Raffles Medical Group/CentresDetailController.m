//
//  CentresDetailController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/7/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "CentresDetailController.h"
#define METERS_PER_MILE 1609.344
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "AddressAnnotation.h"
#import "MapViewController.h"
#import "OperationHourCell.h"
#import "OperationHourModel.h"

@interface CentresDetailController () {
    UIActivityIndicatorView *loadingIndicator;
    NSMutableDictionary *daysDataDic;
    NSMutableArray *daysData;
    NSMutableArray *otherOperationHour;
    NSMutableArray *otherOperationHourData;
}

@end

@implementation CentresDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    otherOperationHourData = [[NSMutableArray alloc] init];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_back"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 15, 15);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    NSString *image = self.categoryImage;
    
    if (![image isEqualToString:@""]) {
        image = [image stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [self.headerImageView setImageWithURL:[NSURL URLWithString:image] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }else {
        [self.headerImageView setImage:[UIImage imageNamed:@"banner"]];
    }
    
    self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.navigationItem.title = [self.data valueForKey:@"CLINICNAME"];
    self.clinicNameLabel.text = [self.data valueForKey:@"CLINICNAME"];
    self.addressLabel.text = [self.data valueForKey:@"ADDRESS"];
    
    if ([self.data valueForKey:@"FAX"]) {
        self.faxLabel.text = [self.data valueForKey:@"FAX"];
    }else {
        [self.faxLabel setHidden:YES];
        [self.faxTitleLabel setHidden:YES];
    }
    
    if ([self.data valueForKey:@"TELEPHONE"]) {
        self.TelLabel.text = [self.data valueForKey:@"TELEPHONE"];
    }else {
        [self.TelLabel setHidden:YES];
        [self.telTitleLabel setHidden:YES];
    }
    
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-10, (self.view.frame.size.height/2)-80, 20,20)];
    [loadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [loadingIndicator setHidesWhenStopped:YES];
    [self.webview addSubview:loadingIndicator];
    
    NSString *service = [self.data valueForKey:@"SERVICES"];
    if (service == nil) {
        service = @"";
    }
    NSString *htmlString = [NSString stringWithFormat:@"<style> body { font-family:helvetica !important; font-size: 50px !important;} </style> %@",service];
    [self.webview loadHTMLString:htmlString baseURL:nil];
    self.webview.delegate = self;
    [self.webview setScalesPageToFit:YES];
    
    //self.ourServiceLabel.text = [self convertHtmlPlainText:[self.data valueForKey:@"SERVICES"]];
    
    self.callButton.layer.cornerRadius = 5;
    
    NSMutableArray *oneWeekOprHour = [[NSMutableArray alloc] init];
    
    NSString *operatioHour;
    if ([[self.data valueForKey:@"OPHRS_MON"] lowercaseString] == NULL) {
        operatioHour = @"Closed";
    }else {
        operatioHour = [[self.data valueForKey:@"OPHRS_MON"] lowercaseString];
    }
    [oneWeekOprHour addObject:operatioHour];
    operatioHour = [NSString stringWithFormat:@"Monday: %@",operatioHour];
    //self.operatioHourLabel.text = operatioHour;
    
    NSString *tuesHour;
    if ([[self.data valueForKey:@"OPHRS_TUE"] lowercaseString] == NULL) {
        tuesHour = @"Closed";
    }else {
        tuesHour = [[self.data valueForKey:@"OPHRS_TUE"] lowercaseString];
    }
    [oneWeekOprHour addObject:tuesHour];
    tuesHour = [NSString stringWithFormat:@"Tuesday: %@",tuesHour];
    //self.tuesdayLabel.text = tuesHour;
    
    NSString *wedHour;
    if ([[self.data valueForKey:@"OPHRS_WED"] lowercaseString] == NULL) {
        wedHour = @"Closed";
    }else {
        wedHour = [[self.data valueForKey:@"OPHRS_WED"] lowercaseString];
    }
    [oneWeekOprHour addObject:wedHour];
    wedHour = [NSString stringWithFormat:@"Wednesday: %@",wedHour];
    //self.wednesdayLabel.text = wedHour;
    
    NSString *thursHour;
    if ([[self.data valueForKey:@"OPHRS_THURS"] lowercaseString] == NULL) {
        thursHour = @"Closed";
    }else {
        thursHour = [[self.data valueForKey:@"OPHRS_THURS"] lowercaseString];
    }
    [oneWeekOprHour addObject:thursHour];
    thursHour = [NSString stringWithFormat:@"Thursday: %@",thursHour];
    //self.thursdayLabel.text = thursHour;
    
    NSString *friHour;
    if ([[self.data valueForKey:@"OPHRS_FRI"] lowercaseString] == NULL) {
        friHour = @"Closed";
    }else {
        friHour = [[self.data valueForKey:@"OPHRS_FRI"] lowercaseString];
    }
    [oneWeekOprHour addObject:friHour];
    friHour = [NSString stringWithFormat:@"Friday: %@",friHour];
    //self.fridayLabel.text = friHour;
    
    NSString *satHour;
    if ([[self.data valueForKey:@"OPHRS_SAT"] lowercaseString] == NULL) {
        satHour = @"Closed";
    }else {
        satHour = [[self.data valueForKey:@"OPHRS_SAT"] lowercaseString];
    }
    [oneWeekOprHour addObject:satHour];
    satHour = [NSString stringWithFormat:@"Saturday: %@",satHour];
    //self.saturdayLabel.text = satHour;
    
    NSString *sunHour;
    if ([[self.data valueForKey:@"OPHRS_SUN"] lowercaseString] == NULL) {
        sunHour = @"Closed";
    }else {
        sunHour = [[self.data valueForKey:@"OPHRS_SUN"] lowercaseString];
    }
    [oneWeekOprHour addObject:sunHour];
    sunHour = [NSString stringWithFormat:@"Sunday: %@",sunHour];
    //self.sundayLabel.text = sunHour;
    
    int index = 0;
    int condition = 0;
    int kind = 0;
    NSMutableArray *mergeSame = [[NSMutableArray alloc] init];
    NSMutableArray *mergeDif = [[NSMutableArray alloc] init];
    for (id object in oneWeekOprHour) {
        NSString *asd = [oneWeekOprHour objectAtIndex:condition];
        if ([object isEqualToString:asd]) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setValue:object forKey:@"value"];
            [dic setValue:[NSString stringWithFormat:@"%d", index] forKey:@"index"];
            [dic setValue:[NSString stringWithFormat:@"%d", condition] forKey:@"condition"];
            [mergeSame addObject:dic];
        }else {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setValue:object forKey:@"value"];
            [dic setValue:[NSString stringWithFormat:@"%d", index] forKey:@"index"];
            [dic setValue:[NSString stringWithFormat:@"%d", condition] forKey:@"condition"];
            [mergeDif addObject:dic];
            
            kind++;
        }
        index++;
    }
    
    NSArray *daysnameArr = [NSArray arrayWithObjects:@"Mon", @"Tue", @"Wed", @"Thurs", @"Fri", @"Sat", @"Sun", nil];
    
    int x = 0;
    NSString *status = @"yes";
    NSMutableArray *hasil = [[NSMutableArray alloc] init];
    for (id object in mergeSame) {
        if ([[object valueForKey:@"index"] intValue] == x) {
            [hasil addObject:[object valueForKey:@"index"]];
        }else {
            status = @"no";
        }
        x++;
    }
    
    daysDataDic = [[NSMutableDictionary alloc] init];
    daysData = [[NSMutableArray alloc] init];
    
    NSString *firstDay = [daysnameArr objectAtIndex:[[hasil objectAtIndex:0] integerValue]];
    NSString *lastDay;
    
    if ([status isEqual: @"no"]) {
        lastDay = [NSString stringWithFormat:@"%@ & %@",[daysnameArr objectAtIndex:[[hasil objectAtIndex:hasil.count-1] integerValue]],[daysnameArr objectAtIndex:[[[mergeSame valueForKey:@"index"] objectAtIndex:mergeSame.count-1] integerValue]]];
    }else {
        lastDay = [daysnameArr objectAtIndex:[[hasil objectAtIndex:hasil.count-1] integerValue]];
    }

    NSString *value = [[mergeSame valueForKey:@"value"] objectAtIndex:0];
    NSString *days = [NSString stringWithFormat:@"%@ - %@",firstDay,lastDay];
    days = [days stringByReplacingOccurrencesOfString:@"Mon - Mon" withString:@"Mon"];
    //NSString *same = [NSString stringWithFormat:@"%@:%@ \n",days,value];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.operationHourView.frame.size.width/1.3, 20)];
    label1.text = [NSString stringWithFormat:@"%@:",days];
    [daysDataDic setValue:days forKey:@"days"];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(self.operationHourView.frame.size.width/2, 0, self.operationHourView.frame.size.width/2, 20)];
    label2.text = [NSString stringWithFormat:@"%@",value];
    [daysDataDic setValue:value forKey:@"value"];
    
    [daysData addObject:daysDataDic];
    
    for (id object in mergeDif) {
        NSString *day = [daysnameArr objectAtIndex:[[object valueForKey:@"index"] integerValue]];
        NSString *value = [object valueForKey:@"value"];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        
        [dic setValue:day forKey:@"days"];
        [dic setValue:value forKey:@"value"];
        
        [daysData addObject:dic];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if ([self.data valueForKey:@"OPHRS_PH"]) {
        [dic setValue:@"Public Holiday" forKey:@"days"];
        [dic setValue:[[self.data valueForKey:@"OPHRS_PH"] lowercaseString] forKey:@"value"];
        [daysData addObject:dic];
    }
    
    self.operationHourTableview.rowHeight = UITableViewAutomaticDimension;
    self.operationHourTableview.estimatedRowHeight = 44.0;
    
    self.operationHourTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.operationHourTableview setBounces:NO];
    
    CLLocationCoordinate2D zoomLocation;
    double latitude = [[self.data valueForKey:@"LATITUDE"] doubleValue];
    double longitude = [[self.data valueForKey:@"LONGITUDE"] doubleValue];
    
    zoomLocation.latitude = latitude;
    zoomLocation.longitude= longitude;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = zoomLocation;
    
    if (latitude == 0.0 || longitude == 0.0) {
        
//        NSLayoutConstraint *bottom =[NSLayoutConstraint
//                                     constraintWithItem:self.webview
//                                     attribute:NSLayoutAttributeBottom
//                                     relatedBy:NSLayoutRelationEqual
//                                     toItem:self.webview
//                                     attribute:NSLayoutAttributeBottom
//                                     multiplier:1.0f
//                                     constant:0.f];
//        
//        [self.operationHourView addConstraint:bottom];
        [self.locationMapView removeFromSuperview];
        [self.locationLabel removeFromSuperview];
    }else {
        [self.locationMapView addAnnotation:point];
        [self.locationMapView setRegion:viewRegion animated:YES];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapViewTapped)];
        tapGesture.delegate = self;
        [self.locationMapView addGestureRecognizer:tapGesture];
    }

    otherOperationHour = [self.data valueForKey:@"ListOperationDetail"];
    NSArray *TemplateOperatingHoursDetail;
    if (otherOperationHour) {
//        
//        for (id object in otherOperationHour) {
            NSDictionary *TemplateClinicOperatingHoursReturn = [otherOperationHour valueForKey:@"TemplateClinicOperatingHoursReturn"];
    
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            NSDate *dateStart  = [dateFormatter dateFromString:[TemplateClinicOperatingHoursReturn objectForKey:@"EffectiveDateStart"]];
            NSDate *dateEnd  = [dateFormatter dateFromString:[TemplateClinicOperatingHoursReturn objectForKey:@"EffectiveDateEnd"]];
    
            [dateFormatter setDateFormat:@"EEE, MMM d yyyy"];
    
            NSString *efectiveDate;
        NSString *startDate = [dateFormatter stringFromDate:dateStart];
        NSString *endDate = [dateFormatter stringFromDate:dateEnd];
        
        if ([startDate isKindOfClass:[NSNull class]] && [endDate isKindOfClass:[NSNull class]]) {
            efectiveDate = @"New operating hours";
        }else if (startDate && endDate){
            efectiveDate = [NSString stringWithFormat:@"%@ - %@",startDate,endDate];
        }else if (startDate){
            efectiveDate = [NSString stringWithFormat:@"Operating hours from %@",startDate];
        }else if (endDate){
            efectiveDate = [NSString stringWithFormat:@"Operating hours until %@",endDate];
        }
            NSArray *ListOperatignHours = [TemplateClinicOperatingHoursReturn objectForKey:@"ListOperatignHours"];
        
            if([[ListOperatignHours valueForKey:@"TemplateOperatingHoursDetail"] isKindOfClass:[NSArray class]]){
                TemplateOperatingHoursDetail = [ListOperatignHours valueForKey:@"TemplateOperatingHoursDetail"];
            }else if([[ListOperatignHours valueForKey:@"TemplateOperatingHoursDetail"] isKindOfClass:[NSDictionary class]]){
                NSArray *a = [NSArray arrayWithObject:[ListOperatignHours valueForKey:@"TemplateOperatingHoursDetail"]];
                
                TemplateOperatingHoursDetail = a;
            }
        
            OperationHourModel *model = [[OperationHourModel alloc] init];
            
            [model setOperationHourTitle:efectiveDate];
            [model setOperationHour:TemplateOperatingHoursDetail];
            
            [otherOperationHourData addObject:model];
            
//        }
//        
    }
    
    CGFloat height = 45;
    if (TemplateOperatingHoursDetail.count>=1) {
        height *= (daysData.count+1)+(TemplateOperatingHoursDetail.count+1);
    }else {
        height *= daysData.count+1;
    }
    
    CGRect tableFrame = self.operationHourTableview.frame;
    tableFrame.size.height = height;
    self.operationHourTableview.frame = tableFrame;
    //self.heightOperationHour.constant = self.operationHourTableview.frame.size.height;
    self.heightOperationHour.constant = self.operationHourTableview.frame.size.height;
    
    [self.operationHourTableview setScrollEnabled:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mapViewTapped {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapViewController *vc = (MapViewController *)[sb instantiateViewControllerWithIdentifier:@"mapView"];
    
    NSString *latitude = [self.data valueForKey:@"LATITUDE"];
    NSString *longitude = [self.data valueForKey:@"LONGITUDE"];
    
    vc.longitude = longitude;
    vc.latitude = latitude;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (IBAction)callPhone:(id)sender {
    NSString *sPhone = [self.data valueForKey:@"TELEPHONE"];
    
    NSString *newString = [[sPhone componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",newString]]];
}

- (void)goback {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString*)convertHtmlPlainText:(NSString*)HTMLString{
    NSData *HTMLData = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:HTMLData options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:NULL error:NULL];
    NSString *plainString = attrString.string;
    return plainString;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [loadingIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [loadingIndicator stopAnimating];
    CGFloat contentHeight = webView.scrollView.contentSize.height;
    
    self.heightWebView.constant = contentHeight;
}


#pragma mark - Tableview

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (otherOperationHour) {
        return otherOperationHourData.count+1;
    }else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return daysData.count;
    }else{
        OperationHourModel *model;
        model = [otherOperationHourData objectAtIndex:section-1];
        return model.operationHour.count;
    }
        return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"Operation Hour";
    } else {
        OperationHourModel *model;
        model = [otherOperationHourData objectAtIndex:section-1];
        return model.operationHourTitle;
    }
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

//- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
//    [headerView setBackgroundColor:[UIColor clearColor]];
//    
////    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, tableView.bounds.size.width, 20)];
////    
////    if (section == 0) {
////        [title setText:@"Operation Hour"];
////    } else {
////        OperationHourModel *model;
////        model = [otherOperationHourData objectAtIndex:section-1];
////        [title setText:model.operationHourTitle];
////    }
//
////    [headerView addSubview:title];
//    
//    return headerView;
//}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(0, 6, 300, 30);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont boldSystemFontOfSize:16];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    [view addSubview:label];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cell";
    
    OperationHourCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[OperationHourCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        // Configure the cell.
        cell.title.text = [NSString stringWithFormat:@"%@:",[[daysData objectAtIndex:indexPath.row] valueForKey:@"days"]];
        
        cell.value.text = [[daysData objectAtIndex:indexPath.row] valueForKey:@"value"];
        
        if ([[[daysData objectAtIndex:indexPath.row] valueForKey:@"value"] isEqual: @"closed"]) {
            cell.value.text = @"Closed";
        }
        
        if ([[[daysData objectAtIndex:indexPath.row] valueForKey:@"value"] isEqual: @"er only"]) {
            cell.value.text = @"ER Only";
        }
        
        if (![[daysData objectAtIndex:indexPath.row] valueForKey:@"value"]) {
            cell.value.text = @"- ";
        }
    } else {
        
        OperationHourModel *model;
        model = [otherOperationHourData objectAtIndex:indexPath.section-1];
        
        NSString *isdate = [[model.operationHour objectAtIndex:indexPath.row] valueForKey:@"IsDate"];
        
        if ([isdate isEqual:@"true"]) {
            cell.title.text = [[model.operationHour objectAtIndex:indexPath.row] valueForKey:@"Tanggal"];
        }else {
            cell.title.text = [[model.operationHour objectAtIndex:indexPath.row] valueForKey:@"DayName"];
        }
        
        cell.value.text = [[model.operationHour objectAtIndex:indexPath.row] valueForKey:@"OperatingHours"];
    }
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
