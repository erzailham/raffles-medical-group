//
//  OperationHourCell.h
//  Raffles Medical Group
//
//  Created by Reza Ilham on 9/22/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OperationHourCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *value;

@end
