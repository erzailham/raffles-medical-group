//
//  ReadArticleController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/1/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "ReadArticleController.h"
#import "RTLabel.h"
#import "Config.h"
#import "XMLDictionary.h"
#import "WebService.h"

@interface ReadArticleController () {
    RTLabel *label;
    NSMutableArray *data;
    
    WebService *webservice;
    NSMutableDictionary *param;
    UIActivityIndicatorView *loadingIndicator;
}

@end

@implementation ReadArticleController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    webservice = [[WebService alloc] init];
    param = [[NSMutableDictionary alloc] init];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_back"];
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 15, 15);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.titleNavItem.title = self.titleNav;
    
//    label = [[RTLabel alloc] initWithFrame:CGRectMake(8, 8, self.backScroll.frame.size.width, self.backScroll.frame.size.height+200)];
    
    [self getReadArticle];
}

- (void)getReadArticle {
    [webservice getAPI:[NSString stringWithFormat:@"%@/%@",READARTICLE,self.articleID] withParam:param withLoading:YES success:^(NSDictionary *result) {
        if ([result objectForKey:@"Link"] != nil) {
            loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-10, (self.view.frame.size.height/2)-80, 20,20)];
            [loadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
            [loadingIndicator setHidesWhenStopped:YES];
            [self.webview addSubview:loadingIndicator];
            
            NSString *fullURL = [result objectForKey:@"Link"];
            NSURL *url = [NSURL URLWithString:fullURL];
            NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
            [self.webview loadRequest:requestObj];
            self.webview.delegate = self;
        }else {
            loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-10, (self.view.frame.size.height/2)-80, 20,20)];
            [loadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
            [loadingIndicator setHidesWhenStopped:YES];
            [self.webview addSubview:loadingIndicator];
            
            NSString *htmlString;
            if ([[result objectForKey:@"Description"]  isEqual: @"{}"]) {
                NSString *stringHtml = @"";
                htmlString = [NSString stringWithFormat:@"<style> body { font-family:helvetica !important; } </style> %@",stringHtml];
            }
            
            htmlString = [NSString stringWithFormat:@"<style> body { font-family:helvetica !important; } </style> %@",[result objectForKey:@"Description"]];
            
            [self.webview loadHTMLString:htmlString baseURL:nil];
            self.webview.delegate = self;
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toHome:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *vc = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"navHome"];
    [self presentViewController:vc animated:YES completion:NULL];
}

- (IBAction)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [loadingIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [loadingIndicator stopAnimating];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
