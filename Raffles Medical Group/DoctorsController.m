//
//  DoctorsController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/30/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "DoctorsController.h"
#import "XMLDictionary.h"
#import "DetailDoctorController.h"
#import "Config.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface DoctorsController (){
    NSArray *data;
    NSMutableArray *doctor;
    NSMutableArray *dataDoctor;
    NSMutableArray *dataClinicDoctor;
}

@end

@implementation DoctorsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataDoctor = [[NSMutableArray alloc] init];
    doctor = [[NSMutableArray alloc] init];
    dataClinicDoctor = [[NSMutableArray alloc] init];
    
   [self getClinicData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getClinicData {
    
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicData"]];
    NSLog(@"dictionary: %@", xmlDoc);
    
    data  = [xmlDoc objectForKey:@"CLINIC"];
    data  = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SUBCATEGORY_ID == %@)", self.categoryId]];
    
    [self getClicicDoctor];
}

- (void)getClicicDoctor {
    NSArray *doctorID  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicDoctor"];
    for (id object in data) {
        NSArray *filter = [doctorID filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINIC_ID == %@)", [object valueForKey:@"CLINIC_ID"]]];
        
        if ([filter count]>0) {
            [doctor addObjectsFromArray:filter];
        }
    }
    
    [self getDataDoctor];
}

- (void)getDataDoctor {
   data  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListDoctorData"];
    for (id object in doctor) {
        NSString *doctor_id = [object valueForKey:@"DOCTOR_ID"];
        NSArray *filter = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(DOCTOR_ID == %@)", doctor_id]];
        
        if ([filter count]>0) {
            [dataDoctor addObjectsFromArray:filter];
        }
    }
    
    NSArray *doctorID  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicDoctor"];
    NSDictionary *xmlDocs = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicData"]];
    NSArray *doctorClinic = [xmlDocs objectForKey:@"CLINIC"];
    
    for (id object in dataDoctor) {
        NSArray *search = [doctorID filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(DOCTOR_ID == %@)", [object objectForKey:@"DOCTOR_ID"]]];
        
        NSString *clinicId = [[search valueForKey:@"CLINIC_ID"] objectAtIndex:0];
        NSArray *filter = [doctorClinic filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINIC_ID == %@)", clinicId]];
        
        
        [dataClinicDoctor addObject:filter];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataDoctor.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSString *url = [[dataDoctor valueForKey:@"PHOTO"] objectAtIndex:indexPath.row];
    url = [url stringByReplacingOccurrencesOfString:@" "
                                         withString:@"%20"];
    
    [cell.imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"placeholder"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.textLabel.text = [[dataDoctor valueForKey:@"NAME"] objectAtIndex:indexPath.row];
    
    NSDictionary *a = [[dataClinicDoctor objectAtIndex:indexPath.row] objectAtIndex:0];
    
    cell.detailTextLabel.text = [a valueForKey:@"CLINICNAME"];
    
    [cell.detailTextLabel setTextColor:[UIColor colorWithRed:0.04 green:0.35 blue:0.25 alpha:1.0]];
    
    [cell.imageView.layer setCornerRadius:30];
    cell.imageView.layer.masksToBounds = YES;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailDoctorController *vc = (DetailDoctorController *)[sb instantiateViewControllerWithIdentifier:@"detailDoctor"];
    vc.dataDoctor = [dataDoctor objectAtIndex:indexPath.row];
    [self.parentNavigationController pushViewController:vc animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
