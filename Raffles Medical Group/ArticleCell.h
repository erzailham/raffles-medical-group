//
//  ArticcleCell.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/1/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *collapseButton;
@property (weak, nonatomic) IBOutlet UIView *collapseView;
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;

@end
