//
//  PatientTableController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/2/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "PatientTableController.h"
#import "ReadArticleController.h"
#import "ArticleCell.h"
#import "Config.h"
#import "XMLDictionary.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <Chameleon.h>

@interface PatientTableController (){
    NSMutableIndexSet *expandedSections;
    NSMutableArray *data;
    NSMutableArray *TemplateCategorywithArticle;
}

@end

@implementation PatientTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    [self getLatestHappening];
    
    UIImageView *header = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,100)];
    NSString *image = [[NSUserDefaults standardUserDefaults] objectForKey:@"PatientEducation"];
    image = [image stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    [header setImageWithURL:[NSURL URLWithString:image] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    header.contentMode = UIViewContentModeScaleAspectFill;
    [self.tableview setTableHeaderView:header];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getLatestHappening {
    TemplateCategorywithArticle  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListPatientEdu"];
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    for (id object in TemplateCategorywithArticle) {
        NSMutableDictionary *a = [object mutableCopy];
        [a setObject:@"+" forKey:@"TitleButton"];
        [newArray addObject:a];
    }
    
    TemplateCategorywithArticle = newArray;
    
    data = [[TemplateCategorywithArticle valueForKey:@"ListArticle"] valueForKey:@"TemplateArticle"];
}


#pragma mark - Table view data source

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section {
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [TemplateCategorywithArticle count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
             NSInteger i;
            if([[data objectAtIndex:section] isKindOfClass:[NSArray class]]){
                i = [[data objectAtIndex:section] count];
            }else if([[data objectAtIndex:section] isKindOfClass:[NSDictionary class]]){
                i = 1;
            }
            return i+1;
        }
        return 1; // only top row showing
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 54;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if ([self tableView:tableView canCollapseSection:indexPath.section]) {
        if (!indexPath.row) {
            NSArray *a = [data objectAtIndex:indexPath.section];
            if ([a isKindOfClass:[NSNull class]]) {
                [cell.titleLabel setTextColor:[UIColor grayColor]];
                
                cell.collapseButton.hidden = YES;
                cell.collapseView.hidden = YES;
            }else {
                [cell.collapseButton addTarget:self action:@selector(coll:) forControlEvents:UIControlEventTouchUpInside];
                [cell.titleLabel setTextColor:HexColor(@"0A5526")];
                
                cell.collapseButton.hidden = NO;
                cell.collapseView.hidden = NO;
            }
            
            NSString *titleButton = [[TemplateCategorywithArticle valueForKey:@"TitleButton"] objectAtIndex:indexPath.section];
            [cell.collapseButton setTitle:titleButton forState:UIControlStateNormal];
            
            NSString *name = [[TemplateCategorywithArticle valueForKey:@"Name"] objectAtIndex:indexPath.section];
            name = [name stringByReplacingOccurrencesOfString:@"å" withString:@"'"];
            cell.titleLabel.text = name;
        }
        else {
            // all other rows
            if([[data objectAtIndex:indexPath.section] isKindOfClass:[NSArray class]]){
                NSString *name = [[[data objectAtIndex:indexPath.section] valueForKey:@"Nama"] objectAtIndex:indexPath.row-1];
                name = [name stringByReplacingOccurrencesOfString:@"å" withString:@"'"];
                cell.titleLabel.text = name;
            }else if([[data objectAtIndex:indexPath.section] isKindOfClass:[NSDictionary class]]){
                NSString *name = [[data objectAtIndex:indexPath.section] valueForKey:@"Nama"];
                name = [name stringByReplacingOccurrencesOfString:@"å" withString:@"'"];
                cell.titleLabel.text = name;
            }

            [cell.titleLabel setTextColor:[UIColor blackColor]];
            cell.collapseButton.hidden = YES;
            cell.collapseView.hidden = YES;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self tableView:tableView canCollapseSection:indexPath.section]) {
        if (!indexPath.row)
        {
//            // only first row toggles exapand/collapse
//            [tableView deselectRowAtIndexPath:indexPath animated:YES];
//            
//            NSInteger section = indexPath.section;
//            BOOL currentlyExpanded = [expandedSections containsIndex:section];
//            NSInteger rows;
//            
//            NSMutableArray *tmpArray = [NSMutableArray array];
//            
//            if (currentlyExpanded) {
//                rows = [self tableView:tableView numberOfRowsInSection:section];
//                [expandedSections removeIndex:section];
//            }
//            else {
//                [expandedSections addIndex:section];
//                rows = [self tableView:tableView numberOfRowsInSection:section];
//            }
//            
//            for (int i=1; i<rows; i++) {
//                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
//                                                               inSection:section];
//                [tmpArray addObject:tmpIndexPath];
//            }
//            
//            if (currentlyExpanded) {
//                [tableView deleteRowsAtIndexPaths:tmpArray
//                                 withRowAnimation:UITableViewRowAnimationTop];
//            }
//            else {
//                [tableView insertRowsAtIndexPaths:tmpArray
//                                 withRowAnimation:UITableViewRowAnimationTop];
//            }
        }
        else {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ReadArticleController *vc = (ReadArticleController *)[sb instantiateViewControllerWithIdentifier:@"ReadArticle"];
            NSLog(@"aaaaaaa %ld",(long)indexPath.section);
            if([[data objectAtIndex:indexPath.section] isKindOfClass:[NSArray class]]){
                vc.articleID = [[[data valueForKey:@"Kode"] objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1];
            }else if([[data objectAtIndex:indexPath.section] isKindOfClass:[NSDictionary class]]){
                vc.articleID = [[data objectAtIndex:indexPath.section] valueForKey:@"Kode"];
            }
            
            vc.titleNav = @"Patient Education";
            [self.parentNavigationController pushViewController:vc animated:YES];
        }
    }
}

- (void)coll:(UIButton *)sender {
    
    UIView *cellContentView = (UIView *)sender.superview;
    ArticleCell *cell = (ArticleCell *)cellContentView.superview;
    NSIndexPath *indexPath = [self.tableview indexPathForCell:cell];
    NSInteger section = indexPath.section;
    BOOL currentlyExpanded = [expandedSections containsIndex:section];
    NSInteger rows;
    
    NSMutableArray *tmpArray = [NSMutableArray array];
    
    NSString *titleButton = [[TemplateCategorywithArticle valueForKey:@"TitleButton"] objectAtIndex:section];
    if([titleButton isEqual: @"+"]){
        [[TemplateCategorywithArticle objectAtIndex:section] setObject:@"-" forKey:@"TitleButton"];
    }
    else {
        [[TemplateCategorywithArticle objectAtIndex:section] setObject:@"+" forKey:@"TitleButton"];
    }
    
    if (currentlyExpanded) {
        rows = [self tableView:self.tableview numberOfRowsInSection:section];
        [expandedSections removeIndex:section];
    }
    else {
        [expandedSections addIndex:section];
        rows = [self tableView:self.tableview numberOfRowsInSection:section];
    }
    
    for (int i=1; i<rows; i++) {
        NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                       inSection:section];
        [tmpArray addObject:tmpIndexPath];
    }
    
    if (currentlyExpanded) {
        [self.tableview deleteRowsAtIndexPaths:tmpArray
                              withRowAnimation:UITableViewRowAnimationTop];
    }
    else {
        [self.tableview insertRowsAtIndexPaths:tmpArray
                              withRowAnimation:UITableViewRowAnimationTop];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
