//
//  DetailDoctorController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/6/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailDoctorController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *clinicalContent;
@property (weak, nonatomic) IBOutlet UILabel *profileContent;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIWebView *profileWebview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContent;

@property (weak, nonatomic) NSArray *dataDoctor;

@end
