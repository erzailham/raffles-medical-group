//
//  HomeController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/10/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "HomeController.h"
#import "DownPicker.h"
#import "Config.h"
#import "XMLDictionary.h"
#import "FindDoctorController.h"
#import "FndClinicController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WebviewController.h"

@interface HomeController (){
    DownPicker *downPicker;
    DownPicker *downPicker1;
    DownPicker *downPicker2;
    DownPicker *downPicker3;
    
    NSString *first;
    NSString *second;
    NSString *third;
    
    NSMutableArray *dataSpecialty;
    NSMutableArray *dataCentre;
    NSMutableArray *blank;
    NSArray *clinic;
    NSArray *category;
}
@end

@implementation HomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"doctor" forKey:@"statusFind"];
    
    UIImage *image = [UIImage imageNamed:@"logo-text"];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:image];
    [titleImageView setFrame:CGRectMake(0, 0, 20, 20)];
    [titleImageView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = titleImageView;
    
    self.contentView.layer.borderColor = [UIColor blackColor].CGColor;
    self.contentView.layer.borderWidth = 1;
    
    self.firstTextField.placeholder = @"  Search by Doctor Name";
    self.secondTextField.placeholder = @"  Specialty";
    self.thirdTextField.placeholder = @"  Centre";
    
    dataCentre = [[NSMutableArray alloc] init];
    dataSpecialty = [[NSMutableArray alloc] init];
    
    [self getSpeciality];
    [self getCentre];
    
    [self getLocation];
    [self getCategory];
    
    self.secondTextField.hidden = NO;
    self.thirdTextField.hidden = NO;
    self.fourthTextField.hidden = YES;
    self.fiveTextField.hidden = YES;
    
    [self.firstTextField setDelegate:self];
    [self.firstTextField addTarget:self
                       action:@selector(textFieldFinished:)
             forControlEvents:UIControlEventEditingDidEndOnExit];
    
    NSString *back = [[NSUserDefaults standardUserDefaults] objectForKey:@"SearchScreen"];
   
    back = [back stringByReplacingOccurrencesOfString:@" "
                                          withString:@"%20"];
    
    [self.backHome sd_setImageWithURL:[NSURL URLWithString:back]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (void)getSpeciality {
    NSMutableArray *ListSpeciality  = [[NSMutableArray alloc] init];
    [ListSpeciality addObject:@"-- Select Specialty --"];
    [ListSpeciality addObjectsFromArray:[[NSUserDefaults standardUserDefaults] valueForKey:@"ListSpeciality"]];
    downPicker = [[DownPicker alloc] initWithTextField:self.secondTextField withData:ListSpeciality];
    [downPicker setPlaceholder:@"  Specialty"];
}

- (void)getCentre {
    NSArray* docterlist = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicDoctor"];
    docterlist = [docterlist valueForKey:@"CLINIC_ID"];
    
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicData"]];
    //clinic = [xmlDoc objectForKey:@"CLINIC"];
    clinic = [xmlDoc objectForKey:@"CLINIC"];
    
    for (id object in docterlist) {
        NSString *clinic_id = object;
        
        NSArray *cek = [dataCentre filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINIC_ID == %@)", clinic_id]];
        if ([cek count]>0) {
            NSLog(@"%@",clinic_id);
        } else {
            NSArray *filter = [clinic filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINIC_ID == %@)", clinic_id]];
            
            if ([filter count]>0) {
                [dataCentre addObjectsFromArray:filter];
            }
        }
    }
    
    NSMutableArray *data = [[NSMutableArray alloc] init];
    [data addObject:@"-- Select Centre --"];
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:[dataCentre valueForKey:@"CLINICNAME"]];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    [data addObjectsFromArray:arrayWithoutDuplicates];
    downPicker1 = [[DownPicker alloc] initWithTextField:self.thirdTextField withData:data];
    [downPicker1 setPlaceholder:@"  Centre"];
}

- (void)getLocation {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    [data addObject:@"-- Select Location --"];
    [data addObjectsFromArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"AreaClinic7"]];
    downPicker2 = [[DownPicker alloc] initWithTextField:self.fourthTextField withData:data];
    [downPicker2 setPlaceholder:@"  Location"];
}

- (void)getCategory {
    category  = [[NSUserDefaults standardUserDefaults] objectForKey:@"ListCategory"];
    
    NSMutableArray *ListCategory = [[NSMutableArray alloc] init];
    [ListCategory addObject:@"-- Select Service --"];
    for (id object in [category valueForKey:@"CATEGORY"]) {
        [ListCategory addObject:object];
    }
    
    downPicker3 = [[DownPicker alloc] initWithTextField:self.fiveTextField withData:ListCategory];
    [downPicker3 setPlaceholder:@"  Service"];
}

- (IBAction)specialtyTapped:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    self.firstTextField.text = nil;
    
    self.secondTextField.hidden = NO;
    self.thirdTextField.hidden = NO;
    self.fourthTextField.hidden = YES;
    self.fiveTextField.hidden = YES;
    
    self.firstTextField.placeholder = @"  Search by Doctor Name";
    
    [downPicker setText:nil];
    [downPicker1 setText:nil];
    
    [downPicker setPlaceholder:@"  Specialty"];
    [downPicker1 setPlaceholder:@"  Centre"];
    
    self.firstTextField.placeholder = @"  Search by Doctor Name";
    [self.specialtyButton setImage:[UIImage imageNamed:@"ic_specialty_selected"] forState:UIControlStateNormal];
    [self.centreButton setImage:[UIImage imageNamed:@"ic_centre"] forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setObject:@"doctor" forKey:@"statusFind"];
}

- (IBAction)centreTapped:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    self.firstTextField.text = nil;
    
    self.secondTextField.hidden = YES;
    self.thirdTextField.hidden = YES;
    self.fourthTextField.hidden = NO;
    self.fiveTextField.hidden = NO;
    
    self.firstTextField.placeholder = @"  Search by Clinic";
    
    [downPicker2 setText:nil];
    [downPicker3 setText:nil];
    
    [downPicker2 setPlaceholder:@"  Location"];
    [downPicker3 setPlaceholder:@"  Service"];
    
    [self.specialtyButton setImage:[UIImage imageNamed:@"ic_specialty"] forState:UIControlStateNormal];
    [self.centreButton setImage:[UIImage imageNamed:@"ic_centre_selected"] forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setObject:@"centre" forKey:@"statusFind"];
}

- (void)textFieldFinished:(id)sender {
    [sender resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toFacebook:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WebviewController *vc = (WebviewController *)[storyboard instantiateViewControllerWithIdentifier:@"WebviewController"];
    vc.link = @"https://www.facebook.com/RafflesMedGrp";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)toTwitter:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WebviewController *vc = (WebviewController *)[storyboard instantiateViewControllerWithIdentifier:@"WebviewController"];
    vc.link = @"https://twitter.com/RafflesMedGrp";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)toInstagram:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WebviewController *vc = (WebviewController *)[storyboard instantiateViewControllerWithIdentifier:@"WebviewController"];
    vc.link = @"https://www.instagram.com/RafflesMedGrp/";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)toYoutube:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WebviewController *vc = (WebviewController *)[storyboard instantiateViewControllerWithIdentifier:@"WebviewController"];
    vc.link = @"https://www.youtube.com/user/RafflesHospital";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)toLinkedin:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WebviewController *vc = (WebviewController *)[storyboard instantiateViewControllerWithIdentifier:@"WebviewController"];
    vc.link = @"https://sg.linkedin.com/company/raffles-medical-group";
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)searchTapped:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"statusFind"] isEqualToString:@"doctor"]) {
        if (![self.secondTextField.text isEqualToString:@""] || ![self.thirdTextField.text isEqualToString:@""] || ![self.firstTextField.text isEqualToString:@""]) {
            NSArray *filter = [clinic filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINICNAME == %@)", self.thirdTextField.text]];
            
            first = self.firstTextField.text;
            second = self.secondTextField.text;
            
            if ([filter count]>0) {
                third = [[filter valueForKey:@"CLINIC_ID"] objectAtIndex:0];
            }else {
                third = @"";
            }
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            FindDoctorController *vc = (FindDoctorController *)[sb instantiateViewControllerWithIdentifier:@"findDoctor"];
            vc.first = first;
            vc.second = second;
            vc.third = third;
            
            [self.navigationController pushViewController:vc animated:YES];
        }
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"statusFind"] isEqualToString:@"centre"]) {
        if (![self.fourthTextField.text isEqualToString:@""] || ![self.fiveTextField.text isEqualToString:@""] || ![self.firstTextField.text isEqualToString:@""]) {
            NSArray *filter = [category filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CATEGORY == %@)", self.fiveTextField.text]];
            
            first = self.firstTextField.text;
            second = self.fourthTextField.text;
            third =[[filter valueForKey:@"CATEGORY_ID"] componentsJoinedByString:@""];
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            FndClinicController *vc = (FndClinicController *)[sb instantiateViewControllerWithIdentifier:@"FindClinic"];
            vc.first = first;
            vc.second = second;
            vc.third = third;
            
            [self.navigationController pushViewController:vc animated:YES];
        } 
    }
}

@end
