//
//  FndClinicController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/8/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "FndClinicController.h"
#import "CentresDetailController.h"
#import "CentresCell.h"
#import "PageMenuController.h"
#import "XMLDictionary.h"
#import "Config.h"

@interface FndClinicController () {
    NSMutableArray *data;
    NSMutableArray *distanceSortedArray;
    
    CLLocationManager *locationManager;
    
    NSString *currentTime;
    NSDateFormatter *dateFormatter;
    NSDate *now;
    
    BOOL getLocation;
}

@end

@implementation FndClinicController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_back"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 15, 15);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh.mm"];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh.mma"];
    
    currentTime = [outputFormatter stringFromDate:now];
    
    getLocation = YES;
    
    data = [[NSMutableArray alloc] init];
    
    UIImage *image = [UIImage imageNamed:@"logo-text"];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:image];
    [titleImageView setFrame:CGRectMake(0, 0, 20, 20)];
    [titleImageView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = titleImageView;
    
    [self getClinicData];
    
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setEstimatedRowHeight:60];
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getClinicData {
//    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"AreaClinic7String"]];
//    NSDictionary *xmlDoc1 = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"AreaClinic2String"]];
    
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ListClinicData"]];
    NSLog(@"dictionary: %@", xmlDoc);
    
    NSArray *CLINIC = [xmlDoc objectForKey:@"CLINIC"];
    [data addObjectsFromArray:CLINIC];
    
    NSArray *filter;
    if (![self.second isEqualToString:@""]) {
        filter = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(AREA == %@)", self.second]];
        //NSArray *TemplateArea = [[filter valueForKey:@"ListAreaClinic"] valueForKey:@"CLINIC"];
        [data removeAllObjects];
        [data addObjectsFromArray:filter];
    }
    
    NSArray *filterCat;
    if (![self.third isEqualToString:@""]) {
        filterCat = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CATEGORY_ID == %@)", self.third]];
        [data removeAllObjects];
        [data addObjectsFromArray:filterCat];
    }
    
    NSArray *filterName;
    if (![self.first isEqualToString:@""]) {
        filterName = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CLINICNAME CONTAINS[cd] %@)", self.first]];
        [data removeAllObjects];
        [data addObjectsFromArray:filterName];
    }
    
    [self getNearby];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getNearby {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    
    if (getLocation) {
        getLocation = NO;
        [self calculateDistace:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    }
}

- (void)calculateDistace:(double)latitude longitude:(double)longitude {
    CLLocation *placeLocation;
    CLLocation *userLocation;
    
    userLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    distanceSortedArray = [[NSMutableArray alloc] init];
    
    for (id object in data) {
        
        int distance = 0;
        
        double lati = [[object valueForKey:@"LATITUDE"] doubleValue];
        double longi = [[object valueForKey:@"LONGITUDE"] doubleValue];
        placeLocation = [[CLLocation alloc] initWithLatitude:lati longitude:longi];
        distance = [userLocation distanceFromLocation:placeLocation];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic = object;
        
        distance = distance/1000;
        
        [dic setValue:[NSNumber numberWithInt:distance] forKey:@"distance"];
        
        [distanceSortedArray addObject:dic];
        
        NSLog(@"dic=%@",dic );
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [distanceSortedArray sortUsingDescriptors:descriptors];
    
    data = distanceSortedArray;
    
    [self getOperationHour];
}

- (void)getOperationHour {
    
    NSMutableArray *dataoperation = [[NSMutableArray alloc] init];
    NSUInteger count = 0;
    for (id object in data) {
        
        NSString *operationHour;
        
        NSString *day;
        NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
        switch ([component weekday]) {
            case 1:
                //Sunday
                day = @"OPHRS_SUN";
                break;
            case 2:
                //Monday
                day = @"OPHRS_MON";
                break;
            case 3:
                //Tuesday
                day = @"OPHRS_TUE";
                break;
            case 4:
                //Wednesday
                day = @"OPHRS_WED";
                break;
            case 5:
                //Thursday
                day = @"OPHRS_THURS";
                break;
            case 6:
                //Friday
                day = @"OPHRS_FRI";
                break;
            case 7:
                //Saturday
                day = @"OPHRS_SAT";
                break;
            default:
                break;
        }
        
        NSString *a = [[data valueForKey:day] objectAtIndex:count];
        
        if (![a isKindOfClass:[NSNull class]]) {
            if ([a isEqualToString:@"Open 24 hours"] || [a isEqualToString:@"open 24 hours"] || [a isEqualToString:@"Open 24 Hours"] || [a isEqualToString:@"midnight"] || [a isEqualToString:@"Midnight"] || [a isEqualToString:@"12.00mn"] || [a isEqualToString:@"12.00Mn"] || [a isEqualToString:@"12.00MN"] || [a isEqualToString:@"12.00 mn"] || [a isEqualToString:@"12.00 Mn"] || [a isEqualToString:@"12.00 MN"]) {
                operationHour = @"open";
            }else if ([a isEqualToString:@"Closed"]) {
                operationHour = @"closed";
            } else {
                a = [a stringByReplacingOccurrencesOfString:@"," withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"–" withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"?" withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"to" withString:@""];
                a = [a stringByReplacingOccurrencesOfString:@"  " withString:@" "];
                NSString *start = [a substringToIndex:7];
                start = [start stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSString *end = [a substringWithRange:NSMakeRange([a length]-7, 7)];
                end = [end stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                NSDate *startDate = [dateFormatter dateFromString:start];
                NSDate *endDate = [dateFormatter dateFromString:end];
                
                if ([self isTimeOfDate:now betweenStartDate:startDate andEndDate:endDate]) {
                    NSLog(@"TARGET IS INSIDE!");
                    operationHour = @"open";
                }else {
                    NSLog(@"TARGET IS NOT INSIDE!");
                    if (endDate == nil) {
                        operationHour = @"open";
                    }else {
                        operationHour = @"closed";
                    }
                }
            }
        } else {
            operationHour = @"closed";
        }
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic = object;
        [dic setValue:operationHour forKey:@"operationHour"];
        
        [dataoperation addObject:dic];
        count ++;
    }
    
    data = dataoperation;
    [self.tableView reloadData];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return data.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CentresCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
    CentresCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        [cell2.nearbyButton addTarget:self action:@selector(sortNearby) forControlEvents:UIControlEventTouchUpInside];
        [cell2.AlfaButton addTarget:self action:@selector(sortName) forControlEvents:UIControlEventTouchUpInside];
        
        return cell2;
    }else if (indexPath.section == 1) {
        cell1.clinicName.text = [[data valueForKey:@"CLINICNAME"] objectAtIndex:indexPath.row];
        cell1.address.text = [[data valueForKey:@"ADDRESS"] objectAtIndex:indexPath.row];
        
        NSString *dst = [[data valueForKey:@"distance"] objectAtIndex:indexPath.row];
        if (![dst isEqual:[NSNull null]]) {
            NSString *distance = [[[data valueForKey:@"distance"] objectAtIndex:indexPath.row] stringValue];
            if ([distance isEqualToString:@"0"]) {
                cell1.distance.hidden = NO;
                cell1.distance.text = @"0km";
            }else {
                cell1.distance.hidden = NO;
                cell1.distance.text = [NSString stringWithFormat:@"%@km",[[[data valueForKey:@"distance"] objectAtIndex:indexPath.row] stringValue]];
            }
        }
        
        NSString *opr = [[data valueForKey:@"operationHour"] objectAtIndex:indexPath.row];
        if (![opr isEqual:[NSNull null]]) {
            cell1.trafictImage.image = [UIImage imageNamed:opr];
        }

        return cell1;
    }
    
    return cell1;
}

- (NSDate *)dateByNeutralizingDateComponentsOfDate:(NSDate *)originalDate {
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    // Get the components for this date
    NSDateComponents *components = [gregorian components:  (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate: originalDate];
    
    // Set the year, month and day to some values (the values are arbitrary)
    [components setYear:2000];
    [components setMonth:1];
    [components setDay:1];
    
    return [gregorian dateFromComponents:components];
}

- (BOOL)isTimeOfDate:(NSDate *)targetDate betweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate {
    if (!targetDate || !startDate || !endDate) {
        return NO;
    }
    
    // Make sure all the dates have the same date component.
    NSDate *newStartDate = [self dateByNeutralizingDateComponentsOfDate:startDate];
    NSDate *newEndDate = [self dateByNeutralizingDateComponentsOfDate:endDate];
    NSDate *newTargetDate = [self dateByNeutralizingDateComponentsOfDate:targetDate];
    
    // Compare the target with the start and end dates
    NSComparisonResult compareTargetToStart = [newTargetDate compare:newStartDate];
    NSComparisonResult compareTargetToEnd = [newTargetDate compare:newEndDate];
    
    return (compareTargetToStart == NSOrderedDescending && compareTargetToEnd == NSOrderedAscending);
}

- (void)sortNearby {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [distanceSortedArray sortUsingDescriptors:descriptors];
    data = distanceSortedArray;
    
    [self.tableView reloadData];
}

- (void)sortName {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"CLINICNAME" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [distanceSortedArray sortUsingDescriptors:descriptors];
    data = distanceSortedArray;
    
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CentresDetailController *vc = (CentresDetailController *)[sb instantiateViewControllerWithIdentifier:@"CentresDetail"];
        vc.data = [data objectAtIndex:indexPath.row];
        NSString *photo = [[data valueForKey:@"PHOTO"] objectAtIndex:indexPath.row];
        
        if (photo != (id)[NSNull null]) {
           vc.categoryImage = [[data valueForKey:@"PHOTO"] objectAtIndex:indexPath.row];
        }else {
           vc.categoryImage = @"";
        }
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
} 

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
