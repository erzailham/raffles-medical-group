//
//  CentresDetailController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/7/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RTLabel.h"

@interface CentresDetailController : UIViewController<UIWebViewDelegate, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightWebView;
@property (weak, nonatomic) IBOutlet UILabel *clinicNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *TelLabel;
@property (weak, nonatomic) IBOutlet UILabel *faxLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet MKMapView *locationMapView;
@property (weak, nonatomic) IBOutlet UILabel *ourServiceLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) NSString *categoryImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightOperationHour;
@property (weak, nonatomic) IBOutlet UIView *operationHourView;

@property (weak, nonatomic) IBOutlet UILabel *hourlabel;
@property (weak, nonatomic) NSArray *data;
@property (weak, nonatomic) IBOutlet UITableView *operationHourTableview;
@property (weak, nonatomic) IBOutlet UILabel *faxTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *telTitleLabel;

@end
