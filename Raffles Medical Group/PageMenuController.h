//
//  PageMenuController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/30/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"

@interface PageMenuController : UIViewController<CAPSPageMenuDelegate>

@property (nonatomic, strong) NSString *titleNavigation;
@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *clinicId;
@property (nonatomic, strong) NSString *categoryImage;

@end
