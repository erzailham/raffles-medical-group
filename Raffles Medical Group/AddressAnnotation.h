//
//  AddressAnnotation.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/29/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AddressAnnotation : NSObject<MKAnnotation> {
    CLLocationCoordinate2D coordinate;
}

@end
