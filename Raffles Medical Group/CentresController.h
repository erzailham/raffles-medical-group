//
//  CentresController.h
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/30/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CentresController : UITableViewController<CLLocationManagerDelegate>

@property (nonatomic, strong) UINavigationController *parentNavigationController;
@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *clicicId;
@property (nonatomic, strong) NSString *categoryImage;

@end
