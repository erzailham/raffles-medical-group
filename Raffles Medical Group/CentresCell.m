//
//  CentresCell.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 5/30/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "CentresCell.h"
#import <Chameleon.h>

@implementation CentresCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.nearbyButton.layer.cornerRadius = 5;
    self.AlfaButton.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)nearbyPressed:(id)sender {
    [self.nearbyButton setBackgroundColor:HexColor(@"075A40")];
    [self.AlfaButton setBackgroundColor:[UIColor grayColor]];
    self.distance.hidden = NO;
}

- (IBAction)alfaPressed:(id)sender {
    [self.nearbyButton setBackgroundColor:[UIColor grayColor]];
    [self.AlfaButton setBackgroundColor:HexColor(@"075A40")];
    self.distance.hidden = YES;
}

@end
