//
//  WebService.m
//  Unilever-Portal-News
//
//  Created by Reza Ilham Mubaroq on 2/4/16.
//  Copyright © 2016 com.unilever. All rights reserved.
//

#import "WebService.h"
#import "AppDelegate.h"
#import "ProgressHUD.h"
#import "XMLDictionary.h"

@implementation WebService

#define PATH @"http://mobileapps.rafflesmedical.com:50060/API"

- (void)postAPI:(NSString *)action withParam:(NSMutableDictionary *)param withLoading:(BOOL)isLoading success:(void (^)(NSDictionary *result))success {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *URL = action;
    
    if (isLoading) {
        [ProgressHUD show:@"Please Wait" Interaction:NO];
    }
    
    operation = [manager POST:URL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     if (isLoading) {
                         [ProgressHUD dismiss];
                     }
                     
                     success(responseObject);
                     
                     NSLog( @"%@",responseObject);
                 }
                      failure:^(AFHTTPRequestOperation *op, NSError *error)
                 {
                     if (isLoading) {
                         [ProgressHUD dismiss];
                     }
                     NSDictionary *parameters = @{@"status":@""};
                     success(parameters);
                 }];
    
    [operation start];
}


- (void)getAPI:(NSString *)action withParam:(NSMutableDictionary *)param withLoading:(BOOL)isLoading success:(void (^)(NSDictionary *result))success {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestSerializer * requestSerializer = [AFHTTPRequestSerializer serializer];
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *URL = action;
    
    if (isLoading) {
        [ProgressHUD show:@"Please Wait" Interaction:NO];
    }
    
    NSString *ua = @"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25";
    [requestSerializer setValue:ua forHTTPHeaderField:@"User-Agent"];
    [requestSerializer setValue:@"application/xml" forHTTPHeaderField:@"Content-type"];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml", nil];
    manager.responseSerializer = responseSerializer;
    manager.requestSerializer = requestSerializer;
    
    operation = [manager GET:URL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
                {
                     if (isLoading) {
                         [ProgressHUD dismiss];
                     }
                    
                    NSData *xmlData = (NSData *)responseObject;
                    //NSString *xmlString = [NSString stringWithCString:[xmlData bytes] encoding:NSISOLatin1StringEncoding];
                    
                    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLData:xmlData];
                    
                    
                    success(xmlDoc);
                 }
                     failure:^(AFHTTPRequestOperation *op, NSError *error)
                 {
                     if (isLoading) {
                         [ProgressHUD dismiss];
                     }
                     NSDictionary *parameters = @{@"status":@""};
                     success(parameters);
                 }];
    
    [operation start];
}


@end
