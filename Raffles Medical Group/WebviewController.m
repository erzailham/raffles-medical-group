//
//  WebviewController.m
//  Raffles Medical Group
//
//  Created by Erza Ilham on 6/2/16.
//  Copyright © 2016 Raffles Medical Group. All rights reserved.
//

#import "WebviewController.h"

@interface WebviewController () {
    UIActivityIndicatorView *loadingIndicator;
}

@end

@implementation WebviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-10, (self.view.frame.size.height/2)-80, 20,20)];
    [loadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [loadingIndicator setHidesWhenStopped:YES];
    [self.webview addSubview:loadingIndicator];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *fullURL;
    NSString *a = self.link;
    if (self.link != nil) {
        fullURL = self.link;
        [self.backButton setTarget:self];
        [self.backButton setAction:@selector(goBack)];
        UIImage *image = [UIImage imageNamed:@"logo-text"];
        UIImageView *titleImageView = [[UIImageView alloc] initWithImage:image];
        [titleImageView setFrame:CGRectMake(0, 0, 20, 20)];
        [titleImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.navigationItem.titleView = titleImageView;
    }else {
        fullURL = @"https://www.rafflesmedicalgroup.com/patient-education/subscribe-for-mobile-app";
        self.backButton.tintColor = [UIColor clearColor];
        self.navigationItem.hidesBackButton = YES;
    }
    
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
    [self.webview loadRequest:requestObj];
    self.webview.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [loadingIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [loadingIndicator stopAnimating];
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
